package com.woblak.cstream.ingester.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CryptoTrade {

    @NonNull
    private String uuid;
    @NonNull
    private long birthTimestamp;
    @NonNull
    private long eventTimestamp;
    @NonNull
    private String code;
    @NonNull
    private String provider;
    @NonNull
    private int replicaNumber;

    @NonNull
    private Market market;
    @NonNull
    private Trade trade;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Market {
        @NonNull
        String exchangeId;
        @NonNull
        String currencyPairId;
        @NonNull
        String marketId;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Trade {
        @NonNull
        String externalId;
        @NonNull
        Long timestampNano;
        @NonNull
        BigDecimal price;
        @NonNull
        BigDecimal amount;
        @NonNull
        BigDecimal amountQuote;
        @NonNull
        String orderSide;
    }
}
