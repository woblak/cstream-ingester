package com.woblak.cstream.ingester.api.message;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class CryptoTradeMsg implements Message<CryptoTrade> {

    public static final String EVENT = "trade-data";
    public static final String VERSION = "1.0";

    private final MessageHeaders headers;
    private final CryptoTrade payload;
}
