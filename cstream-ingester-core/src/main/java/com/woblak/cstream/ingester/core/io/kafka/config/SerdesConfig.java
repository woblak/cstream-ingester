package com.woblak.cstream.ingester.core.io.kafka.config;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.core.util.SerdeBuilder;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SerdesConfig {

    @Bean
    Serde<String> stringSerde() {
        return Serdes.String();
    }

    @Bean
    Serde<Long> longSerde() {
        return Serdes.Long();
    }

    @Bean
    Serde<CryptowatTrade> cryptowatTradeSerde() {
        return SerdeBuilder.create(CryptowatTrade.class);
    }

    @Bean
    Serde<CryptoTrade> cryptoTradeSerde() {
        return SerdeBuilder.create(CryptoTrade.class);
    }
}
