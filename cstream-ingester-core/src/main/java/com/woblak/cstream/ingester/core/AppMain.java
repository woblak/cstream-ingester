package com.woblak.cstream.ingester.core;

import com.woblak.cstream.ingester.core.config.OnExit;
import com.woblak.cstream.ingester.core.io.kafka.streams.builder.KafkaStreamsBuilder;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.DeduplicatorJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.Properties;

@SpringBootApplication
@Slf4j
public class AppMain {

    public static void main(String... args) {
        SpringApplication.run(AppMain.class, args);
    }

    @Profile({"metrics", "prod", "dev"})
    @Bean
    public CommandLineRunner setUp(
            @Qualifier("defaultStreamsProps") Properties defaultStreamsProperties,
            DeduplicatorJob deduplicatorTradeStreamJob
    ) {
        return args -> {
            KafkaStreams streams = KafkaStreamsBuilder.withProps(defaultStreamsProperties)
                    .addJob(deduplicatorTradeStreamJob)
                    .onError((t, e) -> log.error(e.getMessage(), e))
                    .build();

            streams.start();

            OnExit.addShutdownHook(streams::close);
        };
    }
}
