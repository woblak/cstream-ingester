package com.woblak.cstream.ingester.core.io.kafka.streams.job;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;

public interface SourceStream<K, V> {

    KStream<K, V> streamSource(StreamsBuilder sb);
}
