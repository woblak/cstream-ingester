package com.woblak.cstream.ingester.core.io.kafka.config;

import org.apache.kafka.streams.state.RocksDBConfigSetter;
import org.rocksdb.BlockBasedTableConfig;
import org.rocksdb.Options;
import org.springframework.context.annotation.Profile;

import java.util.Map;

@Profile("dev")
public class RocksDBConfig implements RocksDBConfigSetter {

    public static long blockCacheSize = 50*1024L;
    public static long blockSize = 4096L;
    public static boolean cacheIndexAndFilterBlock = false;


    @Override
    public void setConfig(String storeName, Options options, Map<String, Object> configs) {

        BlockBasedTableConfig tableConfig = new BlockBasedTableConfig();

        tableConfig.setBlockCacheSize(blockCacheSize);
        tableConfig.setBlockSize(blockSize);
        tableConfig.setCacheIndexAndFilterBlocks(cacheIndexAndFilterBlock);

        options.setTableFormatConfig(tableConfig);
        //options.setMaxWriteBufferNumber(2);
    }

}
