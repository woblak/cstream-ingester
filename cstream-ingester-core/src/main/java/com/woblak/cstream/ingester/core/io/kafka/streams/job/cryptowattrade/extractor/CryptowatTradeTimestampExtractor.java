package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.extractor;

import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.mapper.CryptoTradeMapper;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptowatTradeTimestampExtractor implements TimestampExtractor {

    private final CryptoTradeMapper cryptoTradeMapper;

    @Override
    public long extract(ConsumerRecord<Object, Object> consumerRecord, long l) {
        var record = (CryptowatTrade) consumerRecord.value();
        return cryptoTradeMapper.mapToEventTimeStamp(record);
    }
}
