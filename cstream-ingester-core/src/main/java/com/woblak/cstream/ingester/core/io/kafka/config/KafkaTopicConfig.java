package com.woblak.cstream.ingester.core.io.kafka.config;

import com.woblak.cstream.api.KafkaTopics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Profile({"metrics", "prod", "dev"})
@Configuration
@Slf4j
public class KafkaTopicConfig {

    private final String bootstrapAddress;
    private final int partitions;
    private final int replicas;

    @Autowired
    public KafkaTopicConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.topics.crypto-trades.partitions:1}") int partitions,
            @Value("${spring.kafka.topics.crypto-trades.replicas:1}") int replicas
    ) {
        this.bootstrapAddress = bootstrapAddress;
        this.partitions = partitions;
        this.replicas = replicas;
    }

    @Bean
    public NewTopic cryptoTradesTopic() {
        return TopicBuilder.name(KafkaTopics.CRYPTO_TRADES)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = topicConfigs();
        return new KafkaAdmin(configs);
    }

    @Bean
    public Map<String, Object> topicConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }

    @Bean
    public Map<String, Object> topicConfigs(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress
    ) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }
}