package com.woblak.cstream.ingester.core.io.kafka.streams.builder;

import com.woblak.cstream.ingester.core.io.kafka.streams.job.KafkaStreamJob;
import org.apache.kafka.streams.KafkaClientSupplier;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.processor.internals.DefaultKafkaClientSupplier;

import java.util.Properties;

public class KafkaStreamsBuilder {

    private StreamsBuilder streamsBuilder;
    private Properties props;
    private Thread.UncaughtExceptionHandler exceptionHandler;
    private KafkaClientSupplier kafkaClientSupplier = new DefaultKafkaClientSupplier();

    public KafkaStreamsBuilder(StreamsBuilder streamsBuilder, Properties props) {
        this.streamsBuilder = streamsBuilder;
        this.props= props;
    }

    public static KafkaStreamsBuilder withProps(Properties props) {
        return new KafkaStreamsBuilder(new StreamsBuilder(), props);
    }

    public KafkaStreamsBuilder withSupplier(KafkaClientSupplier kafkaClientSupplier) {
        this.kafkaClientSupplier = kafkaClientSupplier;
        return this;
    }

    public KafkaStreamsBuilder addJob(KafkaStreamJob job) {
        this.streamsBuilder = job.registerOn(streamsBuilder);
        return this;
    }

    public KafkaStreamsBuilder onError(Thread.UncaughtExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
        return this;
    }

    public KafkaStreams build() {
        var kafkaStreams = new KafkaStreams(streamsBuilder.build(), props, kafkaClientSupplier);
        if (exceptionHandler != null) {
            kafkaStreams.setUncaughtExceptionHandler(exceptionHandler);
        }
        return kafkaStreams;
    }
}
