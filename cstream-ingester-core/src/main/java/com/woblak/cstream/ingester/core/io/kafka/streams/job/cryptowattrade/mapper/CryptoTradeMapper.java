package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.mapper;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.core.util.CommonUtils;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
public class CryptoTradeMapper {

    public CryptoTrade mapToCryptoTrade(CryptowatTrade record) {
        if (record == null) {
            return null;
        }
        String uuid = mapToUuid(record);
        return CryptoTrade.builder()
                .uuid(uuid)
                .eventTimestamp(mapToEventTimeStamp(record.getTrade().getTimestampNano()))
                .code(record.getCode())
                .provider(record.getProvider())
                .replicaNumber(record.getReplicaNumber())
                .market(mapToMarket(record.getMarket()))
                .trade(mapToTrade(record.getTrade(), uuid))
                .build();
    }

    public Long mapToEventTimeStamp(CryptowatTrade record) {
        return mapToEventTimeStamp(record.getTrade().getTimestampNano());
    }

    private Long mapToEventTimeStamp(Long timestampNano) {
        int maxDigits = 13;
        int nDigits = (int) (Math.log10(timestampNano) + 1);
        int diff = nDigits - maxDigits;
        return diff < 1 ? timestampNano : timestampNano / CommonUtils.POWERS_OF_10[diff];
    }

    private String mapToUuid(CryptowatTrade record) {
        return new StringJoiner("-")
                .add(record.getCode())
                .add(record.getTrade().getTimestampNano().toString())
                .add(record.getMarket().getMarketId())
                .add(record.getTrade().getExternalId())
                .toString();
    }

    private CryptoTrade.Trade mapToTrade(CryptowatTrade.Trade trade, String uuid) {
        if (trade == null) {
            return null;
        }
        return CryptoTrade.Trade.builder()
                .externalId(trade.getExternalId() == null ? uuid : trade.getExternalId())
                .timestampNano(trade.getTimestampNano())
                .amount(trade.getAmount())
                .amountQuote(trade.getAmountQuote())
                .price(trade.getPrice())
                .orderSide(trade.getOrderSide())
                .build();
    }

    private CryptoTrade.Market mapToMarket(CryptowatTrade.Market market) {
        if (market == null) {
            return null;
        }
        return CryptoTrade.Market.builder()
                .exchangeId(market.getExchangeId())
                .currencyPairId(market.getCurrencyPairId())
                .marketId(market.getMarketId())
                .build();
    }
}
