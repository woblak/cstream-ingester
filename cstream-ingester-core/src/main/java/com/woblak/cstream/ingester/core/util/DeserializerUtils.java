package com.woblak.cstream.ingester.core.util;

import lombok.experimental.UtilityClass;
import org.apache.kafka.common.header.Headers;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.messaging.MessageHeaders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class DeserializerUtils {

    public static String byteToString(byte[] bytes) {
        String s;

        try (
                var bis = new ByteArrayInputStream(bytes);
                var ois = new ObjectInputStream(bis)
        ) {
            s = (String) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new UnsupportedOperationException(e);
        }

        return s;
    }

    public static Map<String, Object> headersToHeadersMap(Headers headers) {
        Map<String, Object> result = new HashMap<>();
        var mapper = new DefaultKafkaHeaderMapper();
        mapper.toHeaders(headers, result);
        return result;
    }

    public static MessageHeaders headerstoMessageHeaders(Headers headers) {
        return new MessageHeaders(headersToHeadersMap(headers));
    }
}
