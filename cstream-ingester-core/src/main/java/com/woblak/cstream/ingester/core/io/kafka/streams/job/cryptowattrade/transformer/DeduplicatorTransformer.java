package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.transformer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class DeduplicatorTransformer<K, V, E> implements ValueTransformerWithKey<K, V, V> {

    private final String storeName;
    private final KeyValueMapper<K, V, E> eventIdExtractor;

    private ProcessorContext context;
    private KeyValueStore<E, Long> timestampStore;

    @Override
    public void init(ProcessorContext processorContext) {
        this.context = processorContext;
        this.timestampStore = (KeyValueStore<E, Long>) processorContext.getStateStore(storeName);
    }

    @Override
    public V transform(K k, V v) {
        E eventId = eventIdExtractor.apply(k, v);
        boolean isDistinct = isDistinct(eventId);

        updateTimestampStore(eventId);

        return isDistinct ? v : null;
    }

    @Override
    public void close() {

    }

    private boolean isDistinct(E eventId) {
        return findLastTimeStamp(eventId).isEmpty();
    }

    private Optional<Long> findLastTimeStamp(E eventId) {
        return Optional.ofNullable(timestampStore.get(eventId));
    }

    private void updateTimestampStore(E eventId) {
        timestampStore.put(eventId, context.timestamp());
    }
}
