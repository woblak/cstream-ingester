package com.woblak.cstream.ingester.core.exception;

public class KafkaSendingFailedException extends RuntimeException {

    public KafkaSendingFailedException(Throwable cause) {
        super(cause);
    }
}
