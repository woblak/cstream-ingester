package com.woblak.cstream.ingester.core.io.kafka.streams.job;

import org.apache.kafka.streams.kstream.KStream;

import java.util.List;

public interface ProcessingStream<K1, V1, K2, V2> {

    KStream<K2, V2> stream(KStream<K1, V1> kStream);
}
