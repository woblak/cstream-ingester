package com.woblak.cstream.ingester.core.io.kafka.config;

import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Properties;

@Profile({"metrics", "prod","dev"})
@Configuration
public class KafkaStreamsProps {

    private final String bootstrapAddress;
    private final String groupId;
    private final String defaultDeserializationExceptionHandler;
    private final int commitIntervalMs;
    private final int cacheMaxMbBufferingConfig;
    private final String processingGuarantee;

    @Autowired
    public KafkaStreamsProps(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.streams.group-id}") String groupId,
            @Value("${spring.kafka.streams.default.deserialization.exception.handler}")
                    String defaultDeserializationExceptionHandler,
            @Value("${spring.kafka.streams.commit.interval.ms:100}") int commitIntervalMs,
            @Value("${spring.kafka.streams.cache.max.bytes.buffering.config:10}") int cacheMaxMbBufferingConfig,
            @Value("${spring.kafka.streams.processing.guarantee:at_least_once}") String processingGuarantee
    ) {
        this.bootstrapAddress = bootstrapAddress;
        this.groupId = groupId;
        this.defaultDeserializationExceptionHandler = defaultDeserializationExceptionHandler;
        this.commitIntervalMs = commitIntervalMs;
        this.cacheMaxMbBufferingConfig = cacheMaxMbBufferingConfig;
        this.processingGuarantee = processingGuarantee;
    }

    @Bean
    public Properties defaultStreamsProps() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, groupId);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                defaultDeserializationExceptionHandler);
        props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, processingGuarantee);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, commitIntervalMs);
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, cacheMaxMbBufferingConfig * 1024 * 1024L);
        props.put(StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG, RocksDBConfig.class);
        return props;
    }
}