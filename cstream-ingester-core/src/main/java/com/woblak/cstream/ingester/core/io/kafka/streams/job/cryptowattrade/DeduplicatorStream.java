package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.api.message.CryptoTradeMsg;
import com.woblak.cstream.ingester.core.io.kafka.config.KafkaStores;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.ProcessingStream;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.mapper.CryptoTradeMapper;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.transformer.DeduplicatorTransformer;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.transformer.HeadersTransformer;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.validator.CryptoTradeValidator;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class DeduplicatorStream implements ProcessingStream<String, CryptowatTrade, String, CryptoTrade> {

    private static final String STORE_NAME = KafkaStores.RAW_TRADES;

    private final CryptoTradeMapper cryptoTradeMapper;
    private final CryptoTradeValidator cryptoTradeValidator;

    @Override
    public KStream<String, CryptoTrade> stream(KStream<String, CryptowatTrade> kStream) {
        return kStream
                .mapValues(cryptoTradeMapper::mapToCryptoTrade)
                .filter((k, v) -> cryptoTradeValidator.validate(v).isValid)
                .selectKey((k, v) -> v.getCode())
                .transformValues(() -> new DeduplicatorTransformer<>(STORE_NAME, (k, v) -> v.getUuid()), STORE_NAME)
                .transform(() -> new HeadersTransformer<>(CryptoTradeMsg.EVENT, CryptoTradeMsg.VERSION))
                .filter((k, v) -> v != null);
    }
}
