package com.woblak.cstream.ingester.core.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Slf4j
public class AppProps {

    private final int payloadMaxLength;

    public AppProps(
            @Value("${app.payload.max.length:1024}") int payloadMaxLength
    ) {
        this.payloadMaxLength = payloadMaxLength;
    }
}
