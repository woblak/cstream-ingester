package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade;

import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.core.io.kafka.config.KafkaStores;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.SourceStream;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.extractor.CryptowatTradeTimestampExtractor;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CryptowatSource implements SourceStream<String, CryptowatTrade> {

    private static final String storeName = KafkaStores.RAW_TRADES;

    private final CryptowatTradeTimestampExtractor cryptowatTradeTimestampExtractor;

    private final Serde<String> stringSerde;
    private final Serde<Long> longSerde;
    private final Serde<CryptowatTrade> cryptowatTradeSerde;

    @Override
    public KStream<String, CryptowatTrade> streamSource(StreamsBuilder sb) {
        StoreBuilder<KeyValueStore<String, Long>> storeBuilder = getStoreBuilder();
        return sb
                .addStateStore(storeBuilder)
                .stream(KafkaTopics.CRYPTOWAT_TRADES, Consumed.with(stringSerde, cryptowatTradeSerde));
    }

    private StoreBuilder<KeyValueStore<String, Long>> getStoreBuilder() {
        var storeSupplier = Stores.inMemoryKeyValueStore(KafkaStores.RAW_TRADES);
        return Stores.keyValueStoreBuilder(storeSupplier, stringSerde, longSerde);
    }
}
