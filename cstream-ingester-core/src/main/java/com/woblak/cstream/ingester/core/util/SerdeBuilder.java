package com.woblak.cstream.ingester.core.util;

import lombok.experimental.UtilityClass;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

@UtilityClass
public class SerdeBuilder {

    public static <T> Serde<T> create(Class<T> clazz) {
        return Serdes.serdeFrom(new JsonSerializer<>(),
                new JsonDeserializer<>(clazz).trustedPackages("*"));
    }
}
