package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.validator;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.jvalidator.SimpleValidator;
import com.woblak.jvalidator.Validator;
import com.woblak.jvalidator.result.SimpleValidationResult;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.function.Predicate;

@Component
public class CryptoTradeValidator implements Validator<CryptoTrade, SimpleValidationResult> {

    private final SimpleValidator<CryptoTrade> validator;

    public CryptoTradeValidator() {
        this.validator = SimpleValidator.of(CryptoTrade.class)
                .addErrorRule(whenAmountIsNegative, thenMsgAmountIsNegative);
    }

    @Override
    public SimpleValidationResult validate(CryptoTrade cryptoTrade) {
        return validator.validate(cryptoTrade);
    }

    private final Predicate<CryptoTrade> whenAmountIsNegative =
            r -> r.getTrade().getAmount().compareTo(BigDecimal.ZERO) < 0;
    private final String thenMsgAmountIsNegative = "Amount is negative";

}
