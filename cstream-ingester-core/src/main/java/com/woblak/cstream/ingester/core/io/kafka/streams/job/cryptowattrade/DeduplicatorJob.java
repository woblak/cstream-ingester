package com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade;

import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.KafkaStreamJob;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class DeduplicatorJob implements KafkaStreamJob {

    private final CryptowatSource cryptowatSource;
    private final DeduplicatorStream deduplicatorStream;

    private final Serde<String> stringSerde;
    private final Serde<CryptoTrade> cryptoTradeSerde;

    @Override
    public StreamsBuilder registerOn(StreamsBuilder sb) {

        KStream<String, CryptowatTrade> tradeSource = cryptowatSource.streamSource(sb);

        KStream<String, CryptoTrade> distinctTradesStream = deduplicatorStream.stream(tradeSource);
        distinctTradesStream.print(Printed.toSysOut());
        distinctTradesStream.to(KafkaTopics.CRYPTO_TRADES, Produced.with(stringSerde, cryptoTradeSerde));

        return sb;
    }
}
