package com.woblak.cstream.ingester.core.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CommonUtils {

    public static final long[] POWERS_OF_10 = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000,
            1000000000L, 10000000000L, 100000000000L, 1000000000000L, 10000000000000L, 100000000000000L};

    public static String truncateString(String s, int limit) {
        return s != null && limit >= 0 && s.length() > limit ? truncate(s, limit) : s;
    }

    private String truncate(String s, int limit) {
        return s.substring(0, limit) + " ...";
    }
}
