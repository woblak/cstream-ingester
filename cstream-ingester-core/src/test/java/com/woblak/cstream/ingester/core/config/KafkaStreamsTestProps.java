package com.woblak.cstream.ingester.core.config;

import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;

public abstract class KafkaStreamsTestProps {

    public static Properties defaultStreamProps(String bootstrapServersConfig) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test_statefull_app_id");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
        props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                "org.apache.kafka.streams.errors.LogAndContinueExceptionHandler");
        return props;
    }
}
