package com.woblak.cstream.ingester.core.config;

import com.woblak.cstream.ingester.core.io.kafka.producer.KafkaSender;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.messaging.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SpringBootTest
@EmbeddedKafka(ports = 19092, brokerProperties = "num.partitions=1")
@Import({KafkaConfigTest.class})
public class EmbeddedKafkaTest {

    protected final String port = "19092";

    private String[] topics;

    @Autowired
    EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    ConsumerFactory<String, Object> consumerFactory;

    @Autowired
    KafkaSender kafkaSender;

    @Autowired
    DefaultKafkaHeaderMapper kafkaHeaderMapper;

    KafkaMessageListenerContainer<String, Object> container;

    List<MessageListener<String, Object>> listeners = new ArrayList<>();

    List<KafkaStreams> executedKafkaStreams = new ArrayList<>();

    protected void start() {
        this.container = initContainer();
        container.start();
        ContainerTestUtils.waitForAssignment(container, embeddedKafkaBroker.getPartitionsPerTopic());
    }

    protected void stop() {
        stopAllKafkaStreams();
        if (isAlive()) {
            container.stop();
        }
    }

    protected void restart() {
        stop();
        start();
    }

    protected boolean isAlive() {
        return container != null && container.isRunning();
    }

    protected void startKafkaStreams(KafkaStreams streams) {
        executedKafkaStreams.add(streams);
        streams.cleanUp();
        streams.start();
    }

    private void stopAllKafkaStreams() {
        Iterator<KafkaStreams> it = executedKafkaStreams.iterator();
        while (it.hasNext()) {
            it.next().close();
            it.remove();
        }
    }

    protected void addListener(MessageListener<String, Object> listener) {
        listeners.add(listener);
    }

    protected Message<?> buildMessage(Message<?> msg) {
        return msg;
    }

    protected void sendMessage(Message<?> msg) {
        kafkaSender.send(msg);
    }

    protected void consumeMessage(Consumer<String, Object> consumer) {
        embeddedKafkaBroker.consumeFromEmbeddedTopics(consumer, topics);
    }

    protected void setTopics(String... topics) {
        this.topics = topics;
    }

    protected Map<String, Object> toHeadersMap(Headers headers) {
        Map<String, Object> result = new HashMap<>();
        kafkaHeaderMapper.toHeaders(headers, result);
        return result;
    }

    private KafkaMessageListenerContainer<String, Object> initContainer() {
        return initContainer(consumerFactory, createListener());
    }

    private KafkaMessageListenerContainer<String, Object> initContainer(
            ConsumerFactory<String, Object> consumerFactory,
            MessageListener<String, Object> listener
    ) {
        KafkaMessageListenerContainer<String, Object> container =
                new KafkaMessageListenerContainer<>(consumerFactory, new ContainerProperties(topics));
        container.setupMessageListener(listener);
        return container;
    }

    private MessageListener<String, Object> createListener() {
        return msg -> listeners.forEach(listener -> listener.onMessage(msg));
    }
}
