package com.woblak.cstream.ingester.core.config;

import com.woblak.cstream.ingester.core.io.kafka.producer.KafkaSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.Map;

@Configuration
@Slf4j
public class KafkaConfigTest {

    @Autowired
    EmbeddedKafkaBroker embeddedKafkaBroker;

    @Primary
    @Bean("test-kafkaSender")
    public KafkaSender kafkaSender(KafkaTemplate<String, Object> kafkaTemplate, AppProps appProps) {
        return new KafkaSender(kafkaTemplate, appProps);
    }

    @Primary
    @Bean("test-kafkaTemplate")
    public KafkaTemplate<String, Object> kafkaTemplate(ProducerFactory<String, Object> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }

    @Primary
    @Bean("test-producer")
    public Producer<String, Object> producer(ProducerFactory<String, Object> producerFactory) {
        return producerFactory.createProducer();
    }

    @Primary
    @Bean("test-consumer")
    public Consumer<String, Object> consumer(ConsumerFactory<String, Object> consumerFactory) {
        return consumerFactory.createConsumer();
    }

    @Primary
    @Bean("test-producerFactory")
    public ProducerFactory<String, Object> producerFactory(
            EmbeddedKafkaBroker embeddedKafkaBroker,
            @Qualifier("test-keySerializer") Serializer<String> keySerializer,
            @Qualifier("test-valueSerializer") Serializer<Object> valueSerializer
    ) {
        Map<String, Object> configs = KafkaTestUtils.producerProps(embeddedKafkaBroker);
        return new DefaultKafkaProducerFactory<>(configs, keySerializer, valueSerializer);
    }

    @Primary
    @Bean("test-consumerFactory")
    public ConsumerFactory<String, Object> consumerFactory(
            EmbeddedKafkaBroker embeddedKafkaBroker,
            @Qualifier("test-keyDeserializer") Deserializer<String> keyDeserializer,
            @Qualifier("test-valueDeserializer") Deserializer<Object> valueDeserializer
    ) {
        Map<String, Object> configs =
                KafkaTestUtils.consumerProps("test-brokers", "false", embeddedKafkaBroker);
        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        return new DefaultKafkaConsumerFactory<>(configs, keyDeserializer, valueDeserializer);
    }

    @Primary
    @Bean("test-keySerializer")
    public Serializer<String> keySerializer() {
        return new StringSerializer();
    }

    @Primary
    @Bean("test-valueSerializer")
    public Serializer<Object> valueSerializer() {
        return new JsonSerializer<>();
    }

    @Primary
    @Bean("test-keyDeserializer")
    public Deserializer<String> keyDeserializer() {
        return new StringDeserializer();
    }

    @Primary
    @Bean("test-valueDeserializer")
    public Deserializer<Object> valueDeserializer() {
        JsonDeserializer<Object> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new ErrorHandlingDeserializer<>(jsonDeserializer);
    }

    @Bean
    public DefaultKafkaHeaderMapper kafkaHeaderMapper() {
        return new DefaultKafkaHeaderMapper();
    }

}

