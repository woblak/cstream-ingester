package com.woblak.cstream.ingester.core.io.kafka.streams;

import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.api.message.CryptoTradeMsg;
import com.woblak.cstream.ingester.core.config.EmbeddedKafkaTest;
import com.woblak.cstream.ingester.core.config.KafkaStreamsTestProps;
import com.woblak.cstream.ingester.core.io.kafka.streams.builder.KafkaStreamsBuilder;
import com.woblak.cstream.ingester.core.io.kafka.streams.job.cryptowattrade.DeduplicatorJob;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class DeduplicatorTradeStreamJobTest extends EmbeddedKafkaTest {

    @Autowired
    DeduplicatorJob deduplicatorTradeStreamJob;

    BlockingQueue<ConsumerRecord<String, Object>> records;

    @BeforeEach
    void setUp() {
        this.records = new ArrayBlockingQueue<>(64);
        super.setTopics(KafkaTopics.CRYPTOWAT_TRADES, KafkaTopics.CRYPTO_TRADES);
        super.addListener(record -> {
            if (KafkaTopics.CRYPTO_TRADES.equals(record.topic())) {
                records.add(record);
            }
        });
        super.start();

        KafkaStreams streams = KafkaStreamsBuilder
                .withProps(KafkaStreamsTestProps.defaultStreamProps("localhost:" + super.port))
                .addJob(deduplicatorTradeStreamJob)
                .build();
        super.startKafkaStreams(streams);
    }

    @AfterEach
    void tearDown() {
        super.stop();
    }

    @Test
    void shouldDeduplicateRecords() throws InterruptedException {

        // given
        final String bitcoin = "btc/usd";
        final long timestamp1 = 1;
        final long timestamp2 = 2;
        Message<CryptowatTrade> msgDuplicated1 = createCryptowatTradeMsg(bitcoin, 1);
        Message<CryptowatTrade> msgDuplicated2 = createCryptowatTradeMsg(bitcoin, 1);
        Message<CryptowatTrade> msgDistinctive = createCryptowatTradeMsg(bitcoin, 2);
        List<Message<CryptowatTrade>> input = List.of(msgDuplicated1, msgDuplicated2, msgDistinctive);

        // when
        input.forEach(super::sendMessage);

        // then
        await()
                .with()
                .alias("Wait until any message will be received.")
                .given()
                .atMost(60000L, TimeUnit.MILLISECONDS)
                .pollInterval(400, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> records.size() >= 2);

        assertEquals(1, records.stream().map(ConsumerRecord::partition).distinct().count());
        ConsumerRecord<String, Object> record1 = records.take();
        ConsumerRecord<String, Object> record2 = records.take();

        assertEquals(bitcoin, record1.key());
        assertEquals(bitcoin, toCryptoTrade(record1).getCode());
        assertNotNull(toCryptoTrade(record1).getUuid());
        assertNotNull(toCryptoTrade(record2).getUuid());
        assertNotEquals(toCryptoTrade(record1).getUuid(), toCryptoTrade(record2).getUuid());
        assertEquals(String.valueOf(record1.timestamp()),
                new String(record1.headers().lastHeader(KafkaHeaders.LAST_TIMESTAMP).value()));
        assertEquals(CryptoTradeMsg.EVENT,
                new String(record1.headers().lastHeader(KafkaHeaders.EVENT).value()));
        assertEquals(CryptoTradeMsg.VERSION,
                new String(record1.headers().lastHeader(KafkaHeaders.VERSION).value()));
    }

    private Message<CryptowatTrade> createCryptowatTradeMsg(String code, long timestamp) {
        CryptowatTrade payload = createCryptowatTrade(code, timestamp);
        return MessageBuilder.withPayload(payload)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.CRYPTOWAT_TRADES)
                .setHeader(KafkaHeaders.MESSAGE_KEY, code)
                .setHeader(KafkaHeaders.EVENT, CryptoTradeMsg.EVENT)
                .setHeader(KafkaHeaders.VERSION, CryptoTradeMsg.VERSION)
                .build();
    }

    private CryptowatTrade createCryptowatTrade(String key, long timestamp) {
        var market = createCryptowatTradeMarket();
        var trade = craeteCryptowatTrade(timestamp);
        return CryptowatTrade.builder()
                .code(key)
                .provider(key)
                .replicaNumber(0)
                .market(market)
                .trade(trade)
                .build();
    }

    private CryptowatTrade.Market createCryptowatTradeMarket() {
        return CryptowatTrade.Market.builder()
                .currencyPairId("currencyPairId")
                .exchangeId("exchangeId")
                .marketId("marketId")
                .build();
    }

    private CryptowatTrade.Trade craeteCryptowatTrade(long timestamp) {
        return CryptowatTrade.Trade.builder()
                .timestampNano(timestamp)
                .amount(BigDecimal.ONE)
                .amountQuote(BigDecimal.ONE)
                .price(BigDecimal.ONE)
                .externalId("externalId")
                .orderSide("seller")
                .build();
    }

    private CryptoTrade toCryptoTrade(ConsumerRecord<String, Object> record) {
        return (CryptoTrade) record.value();
    }
}
