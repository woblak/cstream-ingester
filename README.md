# cstream-ingester  

## About
Maps, filters and deduplicates raw trades.

## Run  
To run you need:  
- running Kafka on port given in yaml properties file  
- use specific profile for instance: `-Dspring.profiles.active=dev`